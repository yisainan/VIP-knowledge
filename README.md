## 一、资源目录
http://jiansuo.cjtsg.com/index/?path=/I-IT%E4%BC%9A%E5%91%98/

## 二、收费标准（全网最低）

#### 1.普通会员 79 元 （原价99元，已更新11T，持续更新）

内容包含：目录[【AAAAAA】](http://jiansuo.cjtsg.com/category/?path=AAAAAAA)中的20多个主流平台

#### 2.至尊会员 149 元 （原价199元，性价比高，已更新113T，持续更新）

内容包含：目录上看到的所有会员课程，[【AAAAAA】](http://jiansuo.cjtsg.com/category/?path=AAAAAAA)、[IT会员](http://jiansuo.cjtsg.com/category/?path=IT%E4%BC%9A%E5%91%98)、[网赚会员](http://jiansuo.cjtsg.com/category/?path=%E7%BD%91%E8%B5%9A%E4%BC%9A%E5%91%98)、[设计会员](http://jiansuo.cjtsg.com/category/?path=%E8%AE%BE%E8%AE%A1%E4%BC%9A%E5%91%98)、[大师会员](http://jiansuo.cjtsg.com/category/?path=%E5%A4%A7%E5%B8%88%E4%BC%9A%E5%91%98)、[至尊专享](http://jiansuo.cjtsg.com/category/?path=%E8%87%B3%E5%B0%8A%E4%BC%9A%E5%91%98%E4%B8%93%E4%BA%AB)、[亲子会员](http://jiansuo.cjtsg.com/category/?path=%E4%BA%B2%E5%AD%90%E5%A4%A7%E6%B1%87%E6%80%BB) 等等，全网最全，没有之一！

## 三、售后服务

<b><details><summary>1.会员终身免费学习</summary></b>

海量课程，买到就是赚到。内容丰富，适用于各行各业相关人群。一生中总会遇到你需要学习需要新知识的时候，愿为每一位需要学习的您服务。

</details>

<b><details><summary>2.加入会员后不满意，直接退款</summary></b>

加入会员后碰到诸如觉得买贵了、心情不好不想要了、看不懂下载说明、当地限制了网盘登录等等问题，1小时内都可以直接找我退款，敬请各位朋友放心。

</details>

## 四、联系我们

我的微信：**zs62525**

添加后可免费试看任一课程，非诚勿扰！
